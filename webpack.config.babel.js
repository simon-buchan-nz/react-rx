import {join} from 'path';

export default {
  entry: {
    index: [
      'core-js',
      './client/index.html',
      './client',
    ],
  },
  output: {
    path: './dist',
    filename: '[name].js',
  },
  module: {
    loaders: [
      {test: /\.js$/, loader: 'babel?cacheDirectory', exclude: /node_modules/},
      {test: /\.html$/, loader: 'file?name=[name].[ext]', exclude: /node_modules/},
    ],
  },
  devtool: 'source-map-fast',
  devServer: {
    contentBase: join(__dirname, '/dist/'),
  },
};
