import {autobind} from 'core-decorators';
import * as React from 'react';
import {actionsContextTypes} from '../utils';
import LabelledSlider from './labelled-slider';

const PropTypes = React.PropTypes;

const userType = PropTypes.shape({
  name: PropTypes.string.isRequired,
});

class UserList extends React.Component {
  static propTypes = {
    users: PropTypes.arrayOf(userType).isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  render() {
    const {users, value} = this.props;
    const options = users.map(({name}) => (
      <option key={name} value={name}>{name}</option>
    ));
    return (
      <select value={value} onChange={this.handleChange}>
        {options}
      </select>
    );
  }

  @autobind
  handleChange(e) {
    this.props.onChange(e.target.value);
  }
}

App.contextTypes = actionsContextTypes;
App.propTypes = PropTypes.oneOf([{
  loading: PropTypes.bool,
}, {
  users: PropTypes.arrayOf(userType).isRequired,
  userName: PropTypes.string.isRequired,
  weight: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  bmi: PropTypes.number.isRequired,
}]);
export default function App({loading, users, userName, weight, height, bmi}, {actions}) {
  const handleUserChange = actions.userSelected;
  const handleReload = actions.reloadUsers;
  const handleWeightChange = e => actions.weightChange(e.target.value);
  const handleHeightChange = e => actions.heightChange(e.target.value);
  return loading ? <div>Loading</div> : (
    <div>
      <div>
        <label>
          Change user: <UserList users={users} value={userName} onChange={handleUserChange}/>
          <button onClick={handleReload}>Reload</button>
        </label>
      </div>
      <div>Hello, {userName}!</div>
      <div>Your BMI is: {bmi}</div>
      <div>
        <LabelledSlider
          label="Weight"
          unit="kg"
          min={40}
          max={120}
          value={weight}
          onChange={handleWeightChange}
          />
      </div>
      <div>
        <LabelledSlider
          label="Height"
          unit="cm"
          min={140}
          max={210}
          value={height}
          onChange={handleHeightChange}
          />
      </div>
    </div>
  );
}
