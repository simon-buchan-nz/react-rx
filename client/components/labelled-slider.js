import * as React from 'react';

const PropTypes = React.PropTypes;
LabelledSlider.propTypes = {
  label: PropTypes.string.isRequired,
  unit: PropTypes.string,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};
export default function LabelledSlider({label, unit, min, max, value, onChange}) {
  return (
    <label>
      {label} {value}{unit}
      <input
        type="range"
        min={min}
        max={max}
        value={value}
        onChange={onChange}
        />
    </label>
  );
}
