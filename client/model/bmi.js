export default function bmi(weight, height) {
  height *= 0.01;
  return Math.round(weight / (height * height));
}
