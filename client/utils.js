import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Rx from 'rxjs/Rx';

const actionsType = React.PropTypes.objectOf(React.PropTypes.func.isRequired).isRequired;

export const actionsContextTypes = {
  actions: actionsType,
};

export function combineState(state$map) {
  const keys = Object.keys(state$map);
  return Rx.Observable.combineLatest(
    keys.map(key => state$map[key]),
    (...values) => {
      const result = {};
      keys.forEach((key, i) => {
        result[key] = values[i];
      });
      return result;
    });
}

export class ActionsContext extends React.Component {
  static propTypes = {
    actions: actionsType,
    children: React.PropTypes.element,
  };

  render() {
    return React.Children.only(this.props.children);
  }

  getChildContext() {
    return {actions: this.props.actions};
  }
}
ActionsContext.childContextTypes = actionsContextTypes;

export function mountApp(selector, {state$, actions}, render) {
  const element = document.querySelector(selector);

  const view$ = state$.map(state => (
    // eslint-disable-next-line react/jsx-key
    <ActionsContext actions={actions}>
      {render(state)}
    </ActionsContext>
  ));

  view$.subscribe({
    next(view) {
      ReactDOM.render(view, element);
    },
    complete() {
      ReactDOM.unmountComponentAtNode(element);
    },
  });
}
