import * as React from 'react';
import Rx from 'rxjs/Rx';
import {combineState, mountApp} from './utils';
import App from './components/app';
import bmi from './model/bmi';

// Model ==========================================
const users = [
  {name: 'Robert', weight: 70, height: 170},
  {name: 'Stimpy', weight: 57, height: 140},
  {name: 'Beavis', weight: 84, height: 195},
];

function makeAppStore(usersSource$) {
  const reloadUsers$ = new Rx.Subject();
  const nameChange$ = new Rx.Subject();
  const weightChange$ = new Rx.Subject();
  const heightChange$ = new Rx.Subject();

  const users$ = usersSource$.merge(
    reloadUsers$.withLatestFrom(usersSource$, (_, users) => users)
  );

  const currentUser$ = users$
    .first(users => users.length)
    .map(users => users[0].name)
    .merge(nameChange$)
    .combineLatest(users$, (name, users) => (
      users.find(u => u.name === name)
    ));

  const state$ = combineState({
    users: users$,
    userName: currentUser$.map(user => user.name),
    weight: currentUser$.map(user => user.weight).merge(weightChange$),
    height: currentUser$.map(user => user.height).merge(heightChange$),
  }).startWith({
    loading: true,
  });

  return {
    state$,
    actions: {
      reloadUsers() {
        reloadUsers$.next();
      },
      userSelected(name) {
        nameChange$.next(name);
      },
      weightChange(value) {
        weightChange$.next(value);
      },
      heightChange(value) {
        heightChange$.next(value);
      },
    },
  };
}

// Bootstrap =========================================
mountApp(
  '#app',
  makeAppStore(Rx.Observable.of(users).delay(1000)), // fake loading...
  state => (
    <App
      {...state}
      bmi={bmi(state.weight, state.height)}
      />
  ));
